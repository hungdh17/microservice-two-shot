// import React from 'react'
import React, { useState, useEffect } from "react";
import './shoelist.css';

// class Test extends React.Component {
//     onClick(event) {
//        func1();
//        func2();
//     }
//     render() {
//        return (
//           <a href="#" onClick={this.onClick}>Test Link</a>
//        );
//     }
//  }
function ShoeList(){
    const [shoes, setShoes] = useState([]);
  // const []

  const getShoes = async () =>{
    const url= 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok){
      const data = await response.json();
      setShoes(data.shoes);
    }
  }

  useEffect(() => {
    getShoes();
  }, [])



    const deleteShoe = async (id) => {
        const shoeUrl = `http://localhost:8080/api/shoes/${id}`;


        // const response = await fetch(shoeUrl, {method: "DELETE"});
        // const refreshPage = () => {
        //     window.location.reload();
        // } 
        
        // const response = await fetch(shoeUrl, {method: "DELETE"}).then(() => {
        //     window.location.reload();
        // }); 

        const response = await fetch(shoeUrl, {method: "DELETE"}).then(() => {
                getShoes();
            }); 

        console.log('delete response', response);
    };
    // const refreshPage = async () =>{
    //     const refreshUrl = `http://localhost:8080/api/shoes/`;
    //     const refreshResponse = await fetch(refreshUrl, {method: "GET"});
    //     console.log('refresh response', refreshResponse)
    // }
    
    // function pageRefresh(){
    //     window.forceUpdate();
    // }
    // function useForceUpdate() {
    //     let [value, setState] = useState(true);
    //     return () => setState(!value);
    // }
    // const response = await fetch(shoeUrl, fetchOptions);
    // if (response.ok){
    //     const something = await response.json();
    //     console.log(something);
    //     setShoes();
    // }
    // }

    return(
        <table className="table table-dark table-hover table-striped">
            <thead>
                <tr>
                    <th>Shoe Name</th>
                    <th>Manufacturer</th>
                    <th>Shoe Picture</th>
                    <th>Shoe Color</th>
                    <th>Shoe Location</th>
                    <th>Delete?</th>
                    {/* <th>Delete Shoe?</th> */}
                </tr>
            </thead>
            <tbody>
                {/* { shoes && shoes.map(shoe => { */}
                {/* { shoes ?.map(shoe => { */}
                { shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            {/* <tr key={shoe.href}></tr>     */}
                            <td>{shoe.name}</td>
                            <td>{shoe.manufacturer}</td>
                            <td><img className="shoeImage" src={shoe.picture_url} alt={shoe.name} /></td>
                            {/* <td>{shoe.picture_url}</td> */}
                            <td>{shoe.color}</td>
                            <td>{shoe.bin.closet_name}</td>
                            <td>
                                {/* <button className='btn btn-danger' onClick={() => {deleteShoe(shoe.id); refreshPage()}}> */}
                                <button className='btn btn-danger' onClick={() => {deleteShoe(shoe.id)}}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                        
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoeList
{/* <button className='btn btn-danger' onClick={() => deleteShoe(shoe.id)}>
                            Delete
                        </button> */}