import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatList from './hatlist';
import HatForm from './hatform';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
function App() {
  const [shoes, setShoes] = useState([]);
  // const []

  const getShoes = async () =>{
    const url= 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok){
      const data = await response.json();
      setShoes(data.shoes);
    }
  }

  useEffect(() => {
    getShoes();
  }, [])
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatList />} />
          <Route path="/hats/create-hat" element={<HatForm />} />
          <Route exact path="shoes">
            {/* <Route index element={<ShoeList shoes={shoes} />} /> */}
            <Route index element={<ShoeList />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
  </BrowserRouter>
  );
}

export default App;
