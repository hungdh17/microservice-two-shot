import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [form, setForm] = useState({
        style_name: '',
        color: '',
        fabric: '',
        image_url: '',
        location: '',
    });

    const navigate = useNavigate();

    const [isSubmitted, setIsSubmitted] = useState(false);
    const [hasError, setHasError] = useState(false);

    useEffect(() => {
        async function fetchLocations() {
            const response = await fetch('http://localhost:8100/api/locations/');
            const data = await response.json();
            setLocations(data.locations);
        }

        fetchLocations();
    }, []);

    function handleChange(e) {
        setForm({
            ...form,
            [e.target.id]: e.target.value,
        });
    }

    async function handleSubmit(e) {
        e.preventDefault();
        const response = await fetch('http://localhost:8090/api/hats/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(form),
        });

        if (response.ok) {
            const data = await response.json();
            alert(`Created hat ${data.id}`);
            setIsSubmitted(true);
            navigate('/hats');
        } else {
            alert('Error creating hat');
            setHasError(true);
        }
    }

    return (
        <div style={{ display: 'flex', justifyContent: 'center', paddingTop: '20px', flexDirection: 'column', alignItems: 'center' }}>
            <div className="jumbotron" style={{ width: '70%', marginBottom: '20px' }}>
                <h1 className="display-2">Let's Add A New Hat!</h1>
                <p className="lead">
                    All of the fields below are required.  Please input all the needed information and let Wardrobify know all the information about your new hat.  You need to choose a location from the available drop down menu.  If you have put your hat into a location that is not in the drop down menu, then you will need to create a new location in the wardrobe first.
                </p>
            </div>
            <form id="contactForm" onSubmit={handleSubmit} style={{ marginTop: '20px', width: '50%' }}>
                <div className="form-floating mb-3">
                    <input className="form-control" id="style_name" type="text" placeholder="Style" onChange={handleChange} required />
                    <label htmlFor="style_name">Style</label>
                    <div className="invalid-feedback" data-sb-feedback="style_name:required">Style is required.</div>
                </div>
                <div className="form-floating mb-3">
                    <input className="form-control" id="color" type="text" placeholder="Color" onChange={handleChange} required />
                    <label htmlFor="color">Color</label>
                    <div className="invalid-feedback" data-sb-feedback="color:required">Color is required.</div>
                </div>
                <div className="form-floating mb-3">
                    <input className="form-control" id="fabric" type="text" placeholder="Fabric" onChange={handleChange} required />
                    <label htmlFor="fabric">Fabric</label>
                    <div className="invalid-feedback" data-sb-feedback="fabric:required">Fabric is required.</div>
                </div>
                <div className="form-floating mb-3">
                    <input className="form-control" id="image_url" type="text" placeholder="Image URL" onChange={handleChange} required />
                    <label htmlFor="image_url">Image URL</label>
                    <div className="invalid-feedback" data-sb-feedback="image_url:required">Image URL is required.</div>
                </div>
                <div className="form-floating mb-3">
                    <select id="location" className="form-control" value={form.location} onChange={handleChange} required>
                        <option value="" disabled>Select a location...</option>
                        {locations.map(location => (
                            <option key={location.id} value={location.id}>
                                {location.closet_name} - Section {location.section_number} / Shelf {location.shelf_number}
                            </option>
                        ))}
                    </select>
                    <label htmlFor="location">Location</label>
                </div>

                {isSubmitted && (
                    <div className="text-center mb-3">
                        <div className="fw-bolder">Form submission successful!</div>
                        <p>Thank you for your submission.</p>
                    </div>
                )}
                {hasError && (
                    <div className="text-center text-danger mb-3">Error sending message!</div>
                )}

                <div className="d-grid">
                    <button className="btn btn-primary btn-lg" id="submitButton" type="submit">Submit</button>
                </div>
            </form>
        </div>
    );
}

export default HatForm;
