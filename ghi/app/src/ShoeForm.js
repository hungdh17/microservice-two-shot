import React, { useState, useEffect } from 'react';

function ShoeForm(){
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [color, setColor] = useState('');
    
    const [bin, setBin] = useState('');

    


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        // console.log('response', response);
        if (response.ok){
            const data = await response.json();
            // console.log('data', data);
            setBins(data.bins);
            console.log('FETCH DATA', data.bins);
        }
    };

    
    
    const handleManufacturerChange = (event) =>{
        const value = event.target.value;
        setManufacturer(value);
    };
    const handleNameChange = (event) =>{
        const value = event.target.value;
        setName(value);
    };
    const handlePictureUrlChange = (event) =>{
        const value = event.target.value;
        setPictureUrl(value);
    };
    const handleColorChange = (event) =>{
        const value = event.target.value;
        setColor(value);
    };
    const handleBinChange = (event) =>{
        // event.preventDefault();
        const value = event.target.value;
        setBin(value);
    };
    const handleSubmit = async (event) =>{
        event.preventDefault();

        const data = {};
        data.name = name;
        data.manufacturer = manufacturer;
        data.picture_url = pictureUrl;
        data.color = color;
        data.bin = bin;
        console.log('DATA', data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "POST",
            // body: JSON.parse(data),
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok){
            const newShoe = await response.json();
            console.log('newShoe', newShoe);
            // setBin([]);
            setBin('');
            setManufacturer('');
            setName('');
            setPictureUrl('');
            setColor('');
            // setBins('');
        }

    }
useEffect(() => {
    fetchData();
}, []);    

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input value={name} 
                        onChange={handleNameChange}
                        placeholder="Name" 
                        required type="text" 
                        name="name" id="name" 
                        className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={manufacturer} onChange={handleManufacturerChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                        <label htmlFor="manufacturer">Manufacturer </label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="pictureUrl" required type="text" name="pictureUrl" id="pictureUrl" className="form-control"/>
                        <label htmlFor="pictureUrl">Picture Url </label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color </label>
                    </div>
                    <div className="mb-3">
                        <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a bin</option>
                        {bins.map(bin =>{
                            return(
                                <option key={bin.id} value={bin.href}>
                                    {bin.closet_name}
                                </option>
                            );
                            })}

                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );

}
export default ShoeForm;