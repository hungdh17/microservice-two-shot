import React from 'react';
import { useNavigate } from 'react-router-dom';
import './hatlist.css';

function HatList() {
    const [hats, setHats] = React.useState([]);
    const navigate = useNavigate();

    async function fetchHats() {
        const response = await fetch('http://localhost:8090/api/hats/');
        const data = await response.json();

        // Fetch each location data individually
        const fetchLocations = data.hats.map(async (hat) => {
            const response = await fetch(`http://localhost:8100/api/locations/${hat.location}`);
            const locationData = await response.json();
            return {...hat, location: locationData};
        });

        const hatsWithLocations = await Promise.all(fetchLocations);
        setHats(hatsWithLocations);
    }

    React.useEffect(() => {
        fetchHats();
    }, []);

    function createHat() {
        navigate('/hats/create-hat');
    }

    async function deleteHat(id) {
      try {
          const response = await fetch(`http://localhost:8090/api/hats/${id}/`, { method: 'DELETE' });
          if (!response.ok) {
              throw new Error(`HTTP error! status: ${response.status}`);
          } else {
              alert(`Hat with ID ${id} was deleted.`);

              fetchHats();
          }
      } catch (error) {
          console.error('An error occurred while deleting the hat:', error);
      }
  }

    function editHat(id) {
        navigate(`/hats/edit/${id}`);
    }

    return (
        <>
          <div className="jumbotron">
            <h1 className="display-2">Look At All The Hats You Have!</h1>
            <p className="lead">
              The list below is all the hats you own! If you bought more hats for some weird reason, you can add them to your wardrobe by clicking the button below!
              If you want to remove a hat from your wardrobe or change its location, click on the edit buttons on the right side of the table!
            </p>
          </div>
          <button onClick={createHat} className="btn btn-lg btn-success">Add New Hat!</button>
          <table className="table table-bordered table-hover table-custom">
            <thead className="table-info">
              <tr>
                <th>Image</th>
                <th>Style</th>
                <th>Color</th>
                <th>Fabric</th>
                <th>Location</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {hats.map((hat) => (
                <tr key={hat.id}>
                  <td><img className="hatImage" src={hat.image_url} alt={hat.style_name} /></td>
                  <td>{hat.style_name}</td>
                  <td>{hat.color}</td>
                  <td>{hat.fabric}</td>
                  <td>{`${hat.location.closet_name} - Section ${hat.location.section_number} / Shelf ${hat.location.shelf_number}`}</td>
                  <td>
                    <button onClick={() => deleteHat(hat.id)}>Delete</button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      );
    }

    export default HatList;
