from django.contrib import admin
from .models import Shoe, BinVO
# Register your models here.

@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    # pass
    list_display = [
        "manufacturer",
        "name",
        "picture_url",
        "color",
        "bin",
    ]

@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    # pass
    list_display = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]




