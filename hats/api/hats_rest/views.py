from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        'style_name',
        'color',
        'fabric',
        'image_url',
        'location',
        'id',
    ]

    def get_extra_data(self, o):
        return {
            'location': o.location,
        }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": [HatEncoder().default(hat) for hat in hats]},
            safe=False,
        )
    else:
        content = json.loads(request.body)
        hats = Hat.objects.create(
            style_name=content["style_name"],
            color=content["color"],
            fabric=content["fabric"],
            image_url=content["image_url"],
            location=content["location"],
        )
        return JsonResponse(
            HatEncoder().default(hats),
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_hats(request, pk):
    try:
        hat_instance = Hat.objects.get(id=pk)
    except Hat.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response

    if request.method == "GET":
        return JsonResponse(
            HatEncoder().default(hat_instance),
            safe=False
        )
    elif request.method == "DELETE":
        hat_instance.delete()
        return JsonResponse(
            HatEncoder().default(hat_instance),
            safe=False,
        )
    else:  # PUT
        content = json.loads(request.body)
        props = ["style_name", "color", "fabric", "image_url", "location"]
        for prop in props:
            if prop in content:
                setattr(hat_instance, prop, content[prop])
        hat_instance.save()
        return JsonResponse(HatEncoder().default(hat_instance), safe=False)
