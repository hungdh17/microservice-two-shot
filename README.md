# Wardrobify

Team:

* Person 1 - Hung Hoang - Shoes
* Person 2 - Bradley Belcher - Hats

## Design

## Shoes microservice
Hung: 
Approach:
    Backend:
        I started off by building the django backend, creating BinVO and other models/views, and encoders for the views. Also set up polling for the shoes api. Set up the urlpatterns for the shoes backend. Verified that my code worked by using insomnia to test out sending JSON bodies using GET, POST requests. 
        
            Main files include: shoes_rest(models.py, admin.py, models.py, urls.py, views.py)
    Frontend:
        Built out the React frontend. ShoesList and ShoesForm was created with various promises and fetch requests to get the data from the backend database to populate into the frontend. Then I linked ShoesList.js, ShoeForm.js, and Nav.js to populate on App.js .
        Copious amounts of useEffect, usestate, fetch requests, and array mapping used. 
            Main files include: ghi/app/src/(App.js, Nav.js, ShoeForm.js, ShoeList.js) 
Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
